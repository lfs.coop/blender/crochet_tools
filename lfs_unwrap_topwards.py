# LFS Unwrap Faces Topwards, copyright (C) 2022 Les Fées Spéciales
# voeu@les-fees-speciales.coop
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.


bl_info = {
    "name": "Crochet Hook",
    "author": "Les Fées Spéciales",
    "version": (1, 0),
    "blender": (2, 80, 0),
    "location": "",
    "description": "Tools to knit crochet",
    "warning": "",
    "doc_url": "",
    "category": "LFS",
}


import bpy
from mathutils import Vector


def get_sorted_vert_indices(mesh, poly, is_incr=False):
    # Sort vertices based on altitude
    vert_indices = poly.vertices[:]
    sorted_vert_indices = sorted(vert_indices, key=lambda v_i: mesh.vertices[v_i].co.z)

    # We need a special method for increases, because their 2-sided edge is at the top:
    #   4---3
    #  /     \
    # 0---1---2
    # So we sort clockwise from edge 4 and invert
    if is_incr:
        # Get the two highest verts
        v_last, v_prev = sorted_vert_indices[-2:]

        # Verts in a poly are sorted depending on winding order.
        # So, if v_last is right after v_prev (modulo number of verts), that means it's at the top left.
        # Otherwise, we need to revert them so that the new v_last is at the top left.
        if vert_indices.index(v_last) - vert_indices.index(v_prev) not in {1, -len(vert_indices) + 1}:
            v_last, v_prev = v_prev, v_last

        # Get new vertex indices based on v_last.
        # We already know that they're winding in the correct order
        # so we just need to reorder them starting from v0, going up (modulo...)
        vert_indices = poly.vertices[:]
        new_vert_indices = []
        for i in range(len(vert_indices)):
            new_vert_index = vert_indices[(vert_indices.index(v_last) - i) % len(vert_indices)]
            new_vert_indices.append(new_vert_index)
        new_vert_indices.reverse()

    else:
        # Get the two lowest verts
        v0, v1 = sorted_vert_indices[:2]

        # Verts in a poly are sorted depending on winding order.
        # So, if v0 is right before v1 (modulo number of verts), that means it's at the lower left.
        # Otherwise, we need to revert them so that the new v0 is at the lower left.
        if vert_indices.index(v1) - vert_indices.index(v0) not in {1, -len(vert_indices) + 1}:
            v0, v1 = v1, v0

        # Get new vertex indices based on v0.
        # We already know that they're winding in the correct order
        # so we just need to reorder them starting from v0, going up (modulo...)
        vert_indices = poly.vertices[:]
        new_vert_indices = []
        for i in range(len(vert_indices)):
            new_vert_index = vert_indices[(vert_indices.index(v0) + i) % len(vert_indices)]
            new_vert_indices.append(new_vert_index)

    return new_vert_indices


def get_stitch_type(mesh, poly):
    # stitch_type in {'TRI', 'QUAD', 'INCR', 'DECR'}
    if len(poly.vertices) == 3:
        return 'TRI'
    elif len(poly.vertices) == 4:
        return 'QUAD'
    elif len(poly.vertices) == 5:
        # To know which orientation the face has, compare the middle vertex to the others.
        # If it’s closer to the top ones, it’s likely a decrease.
        # If it’s closer to the bottom ones, it’s likely an increase.
        vert_indices = poly.vertices[:]
        sorted_vert_indices = sorted(vert_indices, key=lambda v_i: mesh.vertices[v_i].co.z)
        sorted_vert_heights = [mesh.vertices[i].co.z for i in sorted_vert_indices]

        first_two = (sorted_vert_heights[0] + sorted_vert_heights[1]) / 2.0
        last_two = (sorted_vert_heights[-1] + sorted_vert_heights[-2]) / 2.0
        middle_vert = sorted_vert_heights[2]

        if abs(first_two - middle_vert) < abs(last_two - middle_vert):
            return 'INCR'
        else:
            return 'DECR'

    return None


def get_other_poly_from_edge(mesh, poly, edge):
    found_poly = False
    for other_poly in mesh.polygons:
        if other_poly.index == poly.index:
            continue
        for e_i, edge_key in enumerate(other_poly.edge_keys):
            if (       (edge[0] == edge_key[0] and edge[1] == edge_key[1])
                    or (edge[0] == edge_key[1] and edge[1] == edge_key[0])):
                found_poly = other_poly
                break
        if found_poly:
            break

    if not found_poly:
        return

    return found_poly

def get_adjacent_poly(mesh, poly, destination_poly):
    new_vert_indices = get_sorted_vert_indices(mesh, poly)
    stitch_type = get_stitch_type(mesh, poly)
    # stitch_type in {'TRI', 'QUAD', 'INCR', 'DECR'}

    if destination_poly == "Center":
        return poly
    else:
        if stitch_type == "TRI":
            if destination_poly == "Left":
                edge_indices = 0, 2
            elif destination_poly == "Bottom":
                edge_indices = 0, 1
            elif destination_poly in {"Right", "Top"}:  # Top affected to right arbitrarily
                edge_indices = 1, 2
        elif stitch_type == "QUAD":
            if destination_poly == "Left":
                edge_indices = 0, 3
            elif destination_poly == "Bottom":
                edge_indices = 0, 1
            elif destination_poly == "Right":
                edge_indices = 1, 2
            elif destination_poly == "Top":
                edge_indices = 2, 3
        elif stitch_type == "INCR":
            if destination_poly == "Left":
                edge_indices = 0, 4
            elif destination_poly == "Bottom":
                edge_indices = 0, 1
            elif destination_poly == "Right":
                edge_indices = 2, 3
            elif destination_poly == "Top":
                edge_indices = 3, 4
        elif stitch_type == "DECR":
            if destination_poly == "Left":
                edge_indices = 0, 4
            elif destination_poly == "Bottom":
                edge_indices = 0, 1
            elif destination_poly == "Right":
                edge_indices = 1, 2
            elif destination_poly == "Top":
                edge_indices = 3, 4

        edge = (new_vert_indices[edge_indices[0]], new_vert_indices[edge_indices[1]])
        target_poly = get_other_poly_from_edge(mesh, poly, edge)

        return target_poly


class UV_OT_unwrap_topwards(bpy.types.Operator):
    """Unwrap faces so each occupies the whole UV space, facing in the right direction"""
    bl_idname = "uv.lfs_unwrap_topwards"
    bl_label = "Unwrap Faces Topwards"
    bl_options = {'REGISTER', 'UNDO'}

    # TODO choose axis
    # axis: bpy.props...

    @classmethod
    def poll(self, context):
        return context.object is not None

    def execute(self, context):
        mode = context.object.mode
        bpy.ops.object.mode_set(mode='OBJECT')

        for obj in context.selected_objects:
            if obj.type != 'MESH':
                continue

            mesh = obj.data

            # Create UV layer if not exists
            if not "Faces" in mesh.uv_layers:
                uv_layer = mesh.uv_layers.new(name="Faces", do_init=False)
            else:
                uv_layer = mesh.uv_layers["Faces"]

            for poly in mesh.polygons:
                stitch_type = get_stitch_type(mesh, poly)

                new_vert_indices = get_sorted_vert_indices(mesh, poly, is_incr=(stitch_type=='INCR'))

                # Get a map from vert indices to loop indices for this polygon
                vert_loop_map = {mesh.loops[l_i].vertex_index: l_i for l_i in poly.loop_indices}

                # Get the corresponding loop indices for this poly
                new_loop_indices = [vert_loop_map[v_i] for v_i in new_vert_indices]

                # Assign UVs to each loop based on their index

                if stitch_type == 'TRI':
                    #   2
                    #  / \
                    # 0---1
                    new_coordinates = ((0.0, 0.0),
                                       (1.0, 0.0),
                                       (0.5, 1.0))

                if stitch_type == 'QUAD':
                    # 3---2
                    # |   |
                    # 0---1
                    new_coordinates = ((0.0, 0.0),
                                       (1.0, 0.0),
                                       (1.0, 1.0),
                                       (0.0, 1.0))

                elif stitch_type == 'INCR':
                    #   4---3
                    #  /     \
                    # 0---1---2
                    new_coordinates = ((0.0, 0.25),
                                       (0.5, 0.25),
                                       (1.0, 0.25),
                                       (0.75, 0.75),
                                       (0.25, 0.75))

                elif stitch_type == 'DECR':
                    #    4---3---2
                    #     \     /
                    #      0---1
                    new_coordinates = ((0.25, 0.25),
                                       (0.75, 0.25),
                                       (1.0, 0.75),
                                       (0.5, 0.75),
                                       (0.0, 0.75))

                for v_i, new_co in enumerate(new_coordinates):
                    uv_layer.data[new_loop_indices[v_i]].uv = new_co

        bpy.ops.object.mode_set_with_submode(mode=mode)

        return {'FINISHED'}



class UV_OT_unwrap_tiling_sets(bpy.types.Operator):
    """From a base UV layer, create multiple new layers for adjacent faces, scaling UV to 0 for each face"""
    bl_idname = "uv.lfs_unwrap_tiling_sets"
    bl_label = "Unwrap Tiling UV Sets"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(self, context):
        return context.object is not None

    def execute(self, context):
        for obj in context.selected_objects:
            if obj.type != 'MESH':
                continue

            mesh = obj.data

            # Get existing UV layer
            # TODO UI to choose base UV layer
            base_uv_layer_name = "UVMap"

            if not base_uv_layer_name in mesh.uv_layers:
                continue
            base_uv_layer = mesh.uv_layers[base_uv_layer_name]

            for uv_layer_name in ("Center", "Left", "Bottom", "Right", "Top"):
                                  # "Top_Left", "Top_Right", "Bottom_Left", "Bottom_Right"):
                # Create UV layer if doesn't exist
                if uv_layer_name in mesh.uv_layers:
                    new_uv_layer = mesh.uv_layers[uv_layer_name]
                else:
                    new_uv_layer = mesh.uv_layers.new(name=uv_layer_name, do_init=False)

                # For the new UV layer, assign the base layer's polygon center to each vert in the poly
                for poly in mesh.polygons:
                    if not poly.select:
                        continue

                    directions = uv_layer_name.split("_")
                    if len(directions) == 1:
                        target_poly = get_adjacent_poly(mesh, poly, uv_layer_name)

                    # This won't work, only 8 UV maps allowed... :/
                    # # TODO replace this by a direct lookup from opposite vertex
                    # else:
                    #     target_poly = get_adjacent_poly(mesh, poly, directions[1])
                    #     target_poly = get_adjacent_poly(mesh, target_poly, directions[0])

                    if target_poly is None:
                        continue
                    mean_poly_uv = sum((base_uv_layer.data[l_i].uv for l_i in target_poly.loop_indices),
                                       start=Vector.Fill(2, 0.0)) / len(target_poly.vertices)
                    for loop in poly.loop_indices:
                        new_uv_layer.data[loop].uv = mean_poly_uv

        return {'FINISHED'}


class UV_PT_crochet_hook(bpy.types.Panel):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "LFS"
    bl_label = "Crochet"

    def draw(self, context):
        layout = self.layout

        col = layout.column(align=False)
        col.operator(UV_OT_unwrap_topwards.bl_idname)
        col.operator(UV_OT_unwrap_tiling_sets.bl_idname)


def register():
    bpy.utils.register_class(UV_OT_unwrap_topwards)
    bpy.utils.register_class(UV_OT_unwrap_tiling_sets)
    bpy.utils.register_class(UV_PT_crochet_hook)

def unregister():
    bpy.utils.unregister_class(UV_OT_unwrap_topwards)
    bpy.utils.unregister_class(UV_OT_unwrap_tiling_sets)
    bpy.utils.unregister_class(UV_PT_crochet_hook)

if __name__ == "__main__":
    register()
    bpy.ops.uv.lfs_unwrap_topwards()
